# Laboratoires ASM #
Nom : Fröhlich Prenom : Magali

### Laboratoire 5 ###
Laboratoire Simon Says. 

Le lien suivant montre la forme attendue du pseudocode à rendre avec le laboratoire :
[asm_student/labo5/README.md](https://bitbucket.org/magali_frohlich_heigvd/asm_student/src/de532515057a/labo5/?at=master)

Les instructions du pseudocode sont indiquées en commentaires dans le code finale :
[asm_student/labo5/simonsays.S](https://bitbucket.org/magali_frohlich_heigvd/asm_student/src/de532515057a/labo5/?at=master)